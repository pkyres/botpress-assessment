import './App.css';
import FileExplorer from './FileExplorer';

function App() {
  return (
    <div className="App-content">
      <FileExplorer/>
    </div>
  );
}

export default App;
