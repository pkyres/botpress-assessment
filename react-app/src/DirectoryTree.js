import * as React from 'react';
import TreeView from '@mui/lab/TreeView';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import TreeItem from '@mui/lab/TreeItem';
import axios from 'axios';

const pollIntervalMs = 5000;

class DirectoryTree extends React.Component {
  state = {
    expanded: ['root'],
    id: this.props.id,
    nodeMap: {},
  };

  async componentDidMount() {
    const data = await this.fetchNodeData('');
    const node = this.updateNode(data);
    await this.loadChildren(node.id);
    this.timer = setInterval(() => this.poll(), pollIntervalMs);
  }

  componentWillUnmount() {
    clearInterval(this.timer)
    this.timer = undefined;
  }

  async poll() {
    console.log('Polling data');
    const body = Object.values(this.state.nodeMap).filter((node) => node.isDirectory).map((node) => {
      return { id: node.id === 'root' ? '' : node.id, version: node.version };
    });
    const response = await axios.post(`/directories/poll/${this.props.id}`, body);

    if (response.data.length === 0) {
      return;
    }

    console.log(`${response.data.length} updates found`, );

    const collapse = [];
    for (const update of response.data) {
      const nodeId = update.id || 'root';
      if (update.directory) {
        console.log(`Updating ${nodeId}`, );
        this.updateNode(update.directory);
      } else {
        console.log(`Deleting ${nodeId}`, );
        delete this.state.nodeMap[nodeId];
        collapse.push(nodeId);
      }
    }

    await Promise.all(response.data.map(async (update) => {
      if (update.directory) {
        await this.loadChildren(update.id || 'root');
      }
    }));

    if (collapse.length > 0) {
      this.setState({ expanded: this.state.expanded.filter(id => !collapse.includes(id)) });
    }
  }

  async fetchNodeData(id) {
    console.log(`Fetching data: ${id}`);
    const params = new URLSearchParams({ id });
    const response = await axios.get(`/directories/${this.props.id}`, { params });
    return response.data;
  }

  updateNode(data) {
    const { nodeMap } = this.state;

    for (const child of data.children) {
      nodeMap[child.id] = child;
    }

    const node = { ...data, id: data.id || 'root' };
    nodeMap[node.id] = Object.assign(nodeMap[node.id] || {}, node);
    if (node.id === 'root') {
      this.setState({ root: node });
    } else {
      this.setState({ nodeMap });
    }

    return node;
  }

  async loadChildren(nodeId, loadExtra = true) {
    const node = this.state.nodeMap[nodeId];
    if (!node?.children) {
      return;
    }

    await Promise.all(node.children.map(async (child) => {
      if (child.isDirectory) {
        if (child.children === undefined) {
          // child was never loaded
          const data = await this.fetchNodeData(child.id);
          this.updateNode(data);
        }

        if (loadExtra) {
          // buffer extra layers to reduce lag
          await this.loadChildren(child.id, false);
        }
      }
    }));

    this.setState({ nodeMap: this.state.nodeMap });
  }

  async handleChange(event, nodes) {
    const nodeId = nodes[0];
    console.log(`Node clicked ${nodeId}`);
    this.setState({
      expanded: nodes,
    });

    await this.loadChildren(nodeId);
  }

  render() {
    const { root, expanded } = this.state;
    if (!root) {
      return null;
    }
    const renderTree = (nodes) => {
      // TODO add icons for folder / file
      return (
        <TreeItem key={nodes.id} nodeId={nodes.id} label={nodes.name} className='TreeItem-content'>
          {Array.isArray(nodes.children)
            ? nodes.children.map((node) => renderTree(node))
            : null}
        </TreeItem>
      );
    };
    return (
      <TreeView
        aria-label="file system navigator"
        defaultCollapseIcon={<ExpandMoreIcon />}
        defaultExpandIcon={<ChevronRightIcon />}
        sx={{ flexGrow: 1, maxWidth: 300, overflow: 'auto' }}
        expanded={expanded}
        onNodeToggle={this.handleChange.bind(this)}
      >{renderTree(root)}</TreeView>
    );
  }
}

export default DirectoryTree;
