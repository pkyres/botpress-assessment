import React from 'react';
import axios from 'axios';
import DirectoryTree from './DirectoryTree';

class FileExplorer extends React.Component {
  state = {
    trees: [],
  };

  async componentDidMount() {
    const response = await axios.get('/directories');
    this.setState({ trees: response.data });
  }

  render() {
    const { trees } = this.state;
    if (trees.length === 0) {
      return (<p>No directories are being tracked</p>);
    }

    return (
      <div>
        {trees.map((treeId) => (
          <DirectoryTree key={treeId} id={treeId}/>
        ))}
      </div>
    );
  }
}

export default FileExplorer;
