import { inject } from 'inversify';
import { controller, httpGet, httpPost, queryParam, requestBody, requestParam } from 'inversify-express-utils';
import { DirectoriesManager } from '../components/directoriesManager';
import { Directory, DirectoryUpdate, DirectoryVersion } from '../models/directoryCache';

@controller('/directories')
export class DirectoryController {
  constructor(@inject('DirectoriesManager') private directoriesManager: DirectoriesManager) { }

  @httpGet('/')
  public getRoots(): string[] {
    return this.directoriesManager.directoryKeys;
  }

  @httpGet('/:rootId')
  public get(
    @requestParam('rootId') rootId: string,
    @queryParam('id') id: string
  ): Directory {
    return this.directoriesManager.getDirectoryCache(rootId).getDirectory(id);
  }

  @httpPost('/poll/:rootId')
  public poll(
    @requestParam('rootId') rootId: string,
    @requestBody() body: DirectoryVersion[]
  ): DirectoryUpdate[] {
    return this.directoriesManager.getDirectoryCache(rootId).getUpdatedDirectories(body);
  }
}
