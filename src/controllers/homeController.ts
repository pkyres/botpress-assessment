import { Response } from 'express';
import { controller, httpGet, response } from 'inversify-express-utils';
import path from 'path';

@controller('/')
export class HomeController {
  constructor() { }

  @httpGet('/')
  public async get(@response() res: Response) {
    const p = path.join(__dirname , '../../react-app/build/index.html');
    res.sendFile(p);
    return new Promise((res, rej) => {});
  }
}
