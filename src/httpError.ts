export class HttpError extends Error {
  constructor(message: string, public statusCode = 500, public data: unknown = {}) {
    super(message);
    Object.setPrototypeOf(this, HttpError.prototype);
  }

  public static isHttpError(value: Error): value is HttpError {
    return value instanceof HttpError;
  }
}
