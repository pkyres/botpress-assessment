import { Application, NextFunction, Request, Response } from 'express';
import path from 'path';
import * as http from 'http';
import { Container } from 'inversify';
import { InversifyExpressServer } from 'inversify-express-utils';
import * as express from 'express';
import { Logger } from 'winston';
import { HttpError } from './httpError';

// Register controllers
import './controllers/homeController';
import './controllers/directoryController';

export class Server {
  private readonly logger: Logger;
  private rawServer?: http.Server;

  constructor(
    private readonly container: Container,
    private readonly port: number = 8000,
  ) {
    this.logger = this.container.get<Logger>('Logger');
  }

  private loadMiddlewares(app: Application): void {
    app.use(express.urlencoded({ extended: true, limit: '5mb' }));
    app.use(express.json({ limit: '5mb' }));

    const logMiddleware = (req: Request, res: Response, next: NextFunction): void => {
      const statusCode = res.statusCode;
      this.logger.info('Request recieved', {
        method: req.method,
        route: req.url,
        statusCode
      });
      
      next();
    };
    app.use(logMiddleware);

    const fakeLag = async (req: Request, res: Response, next: NextFunction) => {
      await new Promise(resolve => setTimeout(resolve, 500));
      return next();
    }
    // uncomment to simulate lag
    // app.use(fakeLag);

    app.use(express.static(path.join(__dirname, '../react-app/build')));
  }

  private handleError(app: Application): void {
    app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
      const code = HttpError.isHttpError(err) ? err.statusCode : 500;

      this.logger.log(code >= 500 ? 'error': 'info', 'Unhandled exception during request: ', {
        message: err.message,
        stack: err.stack,
        statusCode: code,
        data: HttpError.isHttpError(err) ? err.data : undefined,
      });

      if (res.headersSent) {
        this.logger.warn('Unhandled exception after response has been sent to the client: ', {
          responseCode: res.statusCode,
        });
        return res;
      }

      return res.status(code).json({
        message: code >= 500 ? 'An unexpected error occurred' : err.message,
      });
    });
  }

  private handlePageNotFound(app: Application): void {
    app.use((req, res) => {
      res.status(404).json({
        message: 'Page not found',
      });
    });
  }

  public async start(): Promise<http.Server> {
    if (this.rawServer) {
      return this.rawServer;
    }

    const server = new InversifyExpressServer(this.container);

    server.setConfig((app) => {
      // Load application middlewares
      this.loadMiddlewares(app);
    });

    // Add top level error handler
    server.setErrorConfig(this.handleError.bind(this));

    const application = server.build();

    // Add 404 page handler
    this.handlePageNotFound(application);

    // Start the server
    this.rawServer = application.listen(this.port, () => {
      this.logger.info(`Server started on port ${this.port}`);
    });

    return this.rawServer;
  }

  public async stop(): Promise<void> {
    if (!this.rawServer) {
      return;
    }

    this.rawServer.close((err) => {
      this.rawServer = undefined;

      if (err) {
        this.logger.error('Error while stopping express server', err);
      }
    });
  }
}
