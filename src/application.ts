import { BindingScopeEnum, Container } from 'inversify';
import * as winston from 'winston';
import * as http from 'http';
import { Server } from './server';
import { DirectoriesManager } from './components/directoriesManager';

export class Application {
  private server: Server;
  public readonly container: Container;
  public readonly logger: winston.Logger;

  constructor() {
    this.container = new Container({ defaultScope: BindingScopeEnum.Singleton });

    this.logger = this.createLogger();
    this.container.bind<winston.Logger>('Logger').toConstantValue(this.logger);
    
    const port = Number(process.env.port);
    this.server = new Server(this.container, isNaN(port) ? undefined : port);
  }

  private createLogger(): winston.Logger {
    const consoleTransport = new winston.transports.Console({
      format: winston.format.combine(
        winston.format.colorize(),
        winston.format.printf((data) => {
          const { timestamp, level, message, ...rest } = data;
          const meta = JSON.stringify(rest || {});
          return `${timestamp}  ${level}: ${message}  ${meta}`;
        })
      ),
    });
    return winston.createLogger({
      level: 'crit',
      defaultMeta: { timestamp: new Date().toISOString() },
      levels: {
        verbose: 0,
        debug: 1,
        info: 2,
        warn: 3,
        error: 4,
        crit: 5
      },
      transports: [consoleTransport]
    });
  }

  private populateContainer(): void {
    this.container.bind<string[]>('Directories').toConstantValue(process.argv.slice(2));
    this.container.bind<DirectoriesManager>('DirectoriesManager').to(DirectoriesManager);
  }

  public async start(): Promise<http.Server> {
    this.populateContainer();
    return this.server.start();
  }

  public async stop(): Promise<void> {
    if (!this.server) {
      return;
    }

    try {
      const dirManager = this.container.get<DirectoriesManager>('DirectoriesManager');
      await Promise.all([
        this.server.stop(),
        dirManager.stop(),
      ]);
    } catch (e) {
      this.logger.error(e);
    }
  }
}
