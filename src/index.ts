import 'reflect-metadata';
import { Application } from './application';

const app = new Application();

async function shutDownApplication(signal: NodeJS.Signals): Promise<void> {
  app.logger.warn(`Received ${signal}, shutting down application`);
  await app.stop();
  process.exit(0);
}

// Listen to SIGTERM and SIGINT to gracefully shut down the application
process.on('SIGTERM', shutDownApplication);
process.on('SIGINT', shutDownApplication);

app.start().catch((e: Error) => {
  app.logger.error(e.message, e);
  process.exit(1);
});
