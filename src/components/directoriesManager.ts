import { DirectoryCache } from '../models/directoryCache';
import { inject, injectable } from 'inversify';
import { Logger } from 'winston';
import { HttpError } from '../httpError';

@injectable()
export class DirectoriesManager {
  private readonly directoryMap = new Map<string, DirectoryCache>();
  public readonly directoryKeys: string[];
  constructor(
    @inject('Directories') directories: string[],
    @inject('Logger') private readonly logger: Logger,
  ) {
    this.logger.debug(`[DirectoriesManager] Attempting to track ${directories.length} root directories`, directories);
    for (const directory of directories) {
      try {
        const dirCache = new DirectoryCache(directory, this.logger);
        this.directoryMap.set(dirCache.id, dirCache);
      } catch (error) {
        this.logger.error('Failed to find directory', { directory, error});
      }
    }

    this.directoryKeys = Array.from(this.directoryMap.keys());
    this.logger.info(`[DirectoriesManager] Tracking ${this.directoryKeys.length} root directories`);
  }

  public getDirectoryCache(rootId: string): DirectoryCache {
    const dirCache = this.directoryMap.get(rootId);
    if (!dirCache) {
      throw new HttpError('Invalid directory rootId', 400, { rootId });
    }
    return dirCache;
  }

  public async stop(): Promise<void> {
    const caches = Array.from(this.directoryMap.values());
    await Promise.all(caches.map((cache) => cache.stop()));
  }
}
