import * as fs from 'fs';
import path from 'path';
import * as uuid from 'uuid';
import { Logger } from 'winston';
import * as os from 'os';
import * as chokidar from 'chokidar';
import { HttpError } from '../httpError';

interface FsObject {
  name: string;
  id: string; // relative path from root
  isDirectory: boolean;
};

export interface Directory extends FsObject {
  version: string; // uuid
  children?: (FsObject | Directory)[];
  isDirectory: true;
};

export interface DirectoryVersion {
  id: string,
  version: string,
};

export interface DirectoryUpdate {
  id: string,
  directory?: Directory
}

export class DirectoryCache {
  private readonly watcher: chokidar.FSWatcher;
  private readonly cacheMap = new Map<string, Directory>();

  public readonly rootAbsolutePath: string;
  public readonly name: string;
  public readonly id: string = uuid.v4();

  constructor(directoryPath: string, private logger: Logger) {
    const homedir = os.homedir();
    let rootPath = directoryPath;
    if (directoryPath.startsWith('~')) {
      rootPath = homedir + rootPath.slice(1);
    }
    this.rootAbsolutePath = this.normalizeSlashes(path.resolve(rootPath));
    this.name = this.getNameFromPath(this.rootAbsolutePath);

    this.watcher = chokidar.watch([], {
      depth: 0,
      ignorePermissionErrors: true,
      followSymlinks: false,
      ignoreInitial: true,
      cwd: this.rootAbsolutePath,
      usePolling: true, // without this Windows locks the folder preventing changes
    });

    this.watcher.on('add', (relativePath) => {
      const normalizedPath = this.normalizeSlashes(relativePath);
      this.logger.verbose(`[DirectoryCache] File ${this.getAbsolutePath(normalizedPath)} has been created`);
      this.addFile(normalizedPath);
    });

    this.watcher.on('unlink', (relativePath) => {
      const normalizedPath = this.normalizeSlashes(relativePath);
      this.logger.verbose(`[DirectoryCache] File ${this.getAbsolutePath(normalizedPath)} has been deleted`);
      this.removeFile(normalizedPath);
    });

    this.watcher.on('addDir', (relativePath) => {
      const normalizedPath = this.normalizeSlashes(relativePath);
      this.logger.verbose(`[DirectoryCache] Directory ${this.getAbsolutePath(normalizedPath)} has been created`);
      this.addDirectory(normalizedPath);
    });

    this.watcher.on('unlinkDir', (relativePath) => {
      const normalizedPath = this.normalizeSlashes(relativePath);
      this.logger.verbose(`[DirectoryCache] Directory ${this.getAbsolutePath(normalizedPath)} has been deleted`);
      this.removeDirectory(normalizedPath);
    });

    this.watcher.on('error', (error) => this.logger.warn('[DirectoryCache] An error occurred while watching', error));

    this.addDirectory('');
  }

  public getDirectory(id: string): Directory {
    const parentPath = this.getParentPath(id);
    const parent = this.cacheMap.get(parentPath);
    if (!parent) {
      // client is outdated
      throw new HttpError('Invalid or outdated directory id', 400, { id });
    }

    try {
      this.addDirectory(id);
    } catch (error) {
      this.logger.debug('[DirectoryCache][getDir] Error adding dir', error);
    }

    const dir = this.cacheMap.get(id);

    if (!dir) {
      // client is outdated
      throw new HttpError('Invalid or outdated directory id', 400, { id });
    }

    return this.cloneWithoutGrandchildren(dir);
  }

  public getUpdatedDirectories(versions: DirectoryVersion[]): DirectoryUpdate[] {
    const updatedDirs: DirectoryUpdate[] = [];

    for (const meta of versions) {
      const node = this.cacheMap.get(meta.id);
      if (!node) {
        // Directory was deleted
        updatedDirs.push({ id: meta.id });
      } else if (node.version !== meta.version) {
        // Directory was updated
        updatedDirs.push({ id: meta.id, directory: this.cloneWithoutGrandchildren(node) });
      }
    }

    return updatedDirs;
  }

  public async stop(): Promise<void> {
    return this.watcher.close();
  }

  private cloneWithoutGrandchildren(dir: Directory): Directory {
    const dirCloneWithoutGrandchildren = { ...dir };
    dirCloneWithoutGrandchildren.children = dirCloneWithoutGrandchildren.children!.map((child) => {
      const { children, ...rest } = child as Directory;
      return { ...rest };
    });
    return dirCloneWithoutGrandchildren;
  }

  private getNameFromPath(id: string): string {
    return id.split('/').pop()!;
  }

  private getParentPath(id: string): string {
    return id.split('/').slice(0, -1).join('/');
  }

  private normalizeSlashes(path: string): string {
    return path.replace(/\\/g, '/');
  }

  private getAbsolutePath(id: string): string {
    return id ? `${this.rootAbsolutePath}/${id}`: this.rootAbsolutePath;
  }

  private addDirectory(id: string): void {
    let dir = this.cacheMap.get(id);
    if (dir?.children !== undefined) {
      return;
    }

    const absolutePath = this.getAbsolutePath(id);
    const items = fs.readdirSync(absolutePath, { withFileTypes: true }).map((item) => {
      const fsObj: any = {
        name: item.name,
        isDirectory: item.isDirectory(),
        id: id ? `${id}/${item.name}` : item.name,
      };

      if (fsObj.isDirectory) {
        fsObj.version = uuid.v4();
        this.cacheMap.set(fsObj.id, fsObj);
      }
      return fsObj as FsObject;
    });
    this.watcher.add(id);

    // use Object.assign so parent is updated
    const newDir = Object.assign(dir || {}, {
      id,
      name: this.getNameFromPath(absolutePath),
      children: items,
      version: uuid.v4(),
      isDirectory: true,
    } as Directory);
    this.cacheMap.set(id, newDir);

    if (!dir && id) {
      // dir doesn't currently exist, push to parent children
      const parentPath = this.getParentPath(id);
      const parent = this.cacheMap.get(parentPath);
      if (parent) {
        parent.version = uuid.v4();
        parent.children!.push(newDir);
      }
    }
  }

  private removeDirectory(id: string): void {
    this.cacheMap.delete(id);

    const parentPath = this.getParentPath(id);
    const parent = this.cacheMap.get(parentPath);

    if (!parent) {
      return;
    }

    parent.version = uuid.v4();
    this.removeContentWithId(parent.children!, id);
  }

  private addFile(id: string): void {
    const parentPath = this.getParentPath(id);
    const parent = this.cacheMap.get(parentPath);

    if (!parent) {
      return;
    }

    parent.version = uuid.v4();
    parent.children!.push({
      name: this.getNameFromPath(id),
      id,
      isDirectory: false,
    });
  }

  private removeFile(id: string): void {
    const parentPath = this.getParentPath(id);
    const parent = this.cacheMap.get(parentPath);

    if (!parent) {
      return;
    }

    parent.version = uuid.v4();
    this.removeContentWithId(parent.children!, id);
  }

  private removeContentWithId(contents: FsObject[], id: string) {
    const index = contents.findIndex((item) => item.id === id);
    contents.splice(index, 1);
  }
}
