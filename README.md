# Botpress Assessment - File Explorer

"vs-code like" file explorer tree view with multiple collapsible sections.

## Running the app

```
npm i
npm run build
npm start -- ~/Desktop/ ./src
```
Or
```
npm i
npm run build
node ./dist ~/Desktop/ ./src
```

Then visit http://localhost:8000/

## Solution description
Node.js Express Inversify TypeScript backend. React material-ui frontend.

The solution was created with performance and lazy loading in mind:

File system directories are only scanned when requested by the frontend. Directory structure is cached in ram. chokidar package is used to watch folders for changes to update cache. 

Frontend only asks backend for folder grandchildren as the user expands grandparent. Frontend polls backend for changes, providing a directory id and version uuid. Backend only responds with updates if directory is outdated or deleted. 

Frontend is never provided with absolute paths, so it does not have unnecessary extra information about the host file system.  

### Routes
GET /directories Responds with the available directory trees.  

GET /directories/:rootId with directory id (relative path from root) query param. Responds with the requested directory data (including version uuid).

POST /directories/poll/:rootId with body containing an array of directory ids and version uuids. Responds with an array of deleted or updated directories.

## Notes
### Shortcuts to save time: 
- Normally I would never use JavaScript over TypeScript for the frontend.
- Long polling or web sockets would be much better than regular polling to get directory updates. 

### Unhandled edge cases:
- Renaming / deleting the root folder is not handled. Could be solved with specialized logic. 
- Restarting the server breaks the FE. Can be solved by using predictable rootIds or forcing a FE refresh on error.
- Folder name too long for UI div. Solution could be to auto-scroll text while hovering with mouse.
- Folder with millions of items. If there’s a need, solution could be pagination / infinite scrolling.

### Improvements required before this PR should be approved:
- Frontend and backend unit and integration tests. 
- Frontend service class to manage api calls.
- Frontend UI improvements (e.g. icons to distinguish folders and files, loading icon when fetching folders that are visible to the user, etc.).
- Add checks to ensure FE doesn't poll when any fetching is already in progress. 
- FE and BE eslint

### Other improvements: 
- Backend cache could buffer one extra layer asynchronously (not while a request is waiting for response) so FE never waits for BE I/O.
- Infinite scroll / pagination for very long folder contents.
- Improve BE logging and remove FE logging (e.g. formatting, writing to console)
- Auto generate OpenAPI docs / swagger and related routes.
- Routes and frontend functionality to download, open, create, rename, move and delete folders and/or files.
